# ic

Testing new kinds of work.

Here it’s a math inline $C=k \cdot G\ : k \in \N$.

Here’s a math block
$$
J_{dc^8} = \frac{5632}{\pi \cdot \Sigma}
$$
Now, topic stuffs:

- One
- Two
    - Avocado
    - Banana
        - Yellow

And a citation to finish:

> Bla, bla, bla.
>
> *FelixPcll*